<section id="banner">
	<ul class="bxslider">
		
		<li style="background-image: url('assets/images/banner-sobre.jpg');">
			<div class="sobrebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<h2>Valter<br/>Battilani<br/>dental beauty</h2>
							<p>Para Battilani experiência da estética dental é uma revolução no estilo de vida. Mais do que técnicas de alta performace, o sobrenatural é resultado de paixão pelo estado de arte.</p>
						</div>
					</div>
				</div>
			</div>
		</li>

	</ul>
</section>
<section id="institucional">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag wow fadeInLeft">
				<img src="assets/images/icone-battilani.png"><br/>
				<h2>O dentista em Cascavel especialista em Estética Dental</h2>
			</div>
		</div>
		<div class="row wow fadeInLeft">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 line-hr wow fadeInLeft">
				<img src="assets/images/line.png" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 wow fadeInLeft">
				<h2>Experiência<br/>compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad deserunt omnis perferendis quia alias saepe assumenda voluptate dolor nulla eos? Blanditiis porro saepe fuga sit cumque laborum reiciendis, aliquid delectus.</p>
				<p><img src="assets/images/aspas.png"></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error sit consectetur architecto. Totam porro dolorem sint, et maxime cumque, magnam ea amet quasi dolor ducimus similique sit, cum possimus veniam.</p>
				<h3>V. Battilani</h3>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 wow fadeInRight">
				<img src="assets/images/battilanis.png" width="100%">
			</div>
		</div>
	</div>
</section>