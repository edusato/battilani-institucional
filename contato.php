<section id="contato">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInLeft">
				<div id="formulario">
					<form method="">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<span>Nome</span>
							<input type="text" name="nome">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<span>E-mail</span>
							<input type="text" name="email">
						</div> 
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<span>Telefone</span>
							<input type="text" name="telefone">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<span>Mensagem</span>
							<textarea name="msg"></textarea>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<input type="submit" value="ENVIAR">
						</div>
					</form>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<div id="dados-contato">
					<h2>Dados Gerais</h2>
					<p><span>CLÍNICA ODONTOLÓGICA EM CASCAVEL</span></p>
					<p>Rua Santa Catarina, 1189, Centro<br/>CEP: 85801-040</p>
					<p>contato@valterbattilani.com.br</p>
					<p>Cascavel -Paraná</p>

					<h2>Atendimento</h2>
					<p><span>AGENDE SEU HORÁRIO</span</p>
					<p>Horário de atendimento:</p>
					<p>Segunda à Sexta das 8h ás 18h<br/>Sábado 8h ás 12h</p>
					<p>Fone: (45) 3224-4848</p>
				</div>
			</div>

		</div>
	</div>
</section>

<section id="mapa">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14468.698630297602!2d-53.4622187!3d-24.9601716!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8386072d6e5213eb!2sConsult%C3%B3rio+De+Psicologia!5e0!3m2!1spt-BR!2sbr!4v1535638783959" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>