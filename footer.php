<footer>
	<div id="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 box-footer wow fadeInLeft">
					<h2>Fique Conectado</h2>
					<p><span>SIGA NOSSAS REDES SOCIAIS</span></p>
					<a href="#"><i class="fab fa-facebook-square"></i></a>
					<a href="#"><i class="fab fa-youtube-square"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 box-footer wow fadeInLeft">
					<h2>Dados Gerais</h2>
					<p><span>CLÍNICA EM CASCAVEL</span</p>
					<p>Rua Santa Catarina, 1189, Centro - CEP: 85801-040</p>
					<p>contato@valterbattilani.com.br</p>
					<p>Cascavel -Paraná</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 box-footer wow fadeInLeft">
					<h2>Atendimento</h2>
					<p><span>AGENDE SEU HORÁRIO</span</p>
					<p>Horário de atendimento:</p>
					<p>Segunda à Sexta das 8h ás 18h<br/>Sábado 8h ás 12h</p>
					<p>Fone: (45) 3224-4848</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 box-footer wow fadeInLeft">
					<h2>Política de Privacidade</h2>
					<p><span>DIREITOS DE IMAGEM E CONTEÚDO</span</p>
					<p>Todo o conteúdo de texto e imagem do site são exclusivos. Não é permitido publicar sem autorização prévia.</p>
				</div>
			</div>
		</div>
	</div>
	<div id="menufooter" class="hidden-xs hidden-sm wow fadeInDown">
		<div id="icon-btl"><img src="assets/images/icone-battilani.png" alt=""></div>
		<div class="container">
			<div class="row">
				<ul>
					<li><a href="#">Sobre Battilani</a></li>
					<li><a href="#">Tratamentos</a></li>
					<li><a href="#">Esthetic Cases</a></li>
					<li><a href="#">Dental Beauty</a></li>
					<li><a href="#">Contato</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
					Todos os direitos reservados. All rights reserved.
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 desenvolvimento wow fadeInRight">
					Desenvolivmento Studio Web | Design Marcelo Baldi
				</div>
			</div>
		</div>
	</div>
</footer>
</section><!-- SECTION SITE -->
<!-- scripts -->
<script src="assets/scripts/components/modernizr/modernizr.js" type="text/javascript"></script>
<script src="assets/scripts/components/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="assets/scripts/components/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/js/jquery.bxslider.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/vendor/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="assets/scripts/components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="assets/scripts/components/wow/dist/wow.min.js"></script>
<script src="assets/scripts/components/app.js" type="text/javascript"></script>
<?php //include('controller.php'); ?>
</body>
</html>