<section id="dicas-home">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 wow fadeInRight">
				<div class="box-dicas">
					<a href="?pag=post">
						<img src="timthumb.php?src=assets/images/img01.jpg&w=470&h=340&q=100" alt="">
						<h3>Trajetória de Sucesso</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam explicabo nisi beatae accusamus sint, dicta dolorem provident doloribus non, laboriosam quidem.</p>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 wow fadeInRight">
				<div class="box-dicas">
					<a href="?pag=post">
						<img src="timthumb.php?src=assets/images/img02.jpg&w=470&h=340&q=100" alt="">
						<h3>Trajetória de Sucesso</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam explicabo nisi beatae accusamus sint, dicta dolorem provident doloribus non, laboriosam quidem.</p>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 wow fadeInRight">
				<div class="box-dicas">
					<a href="?pag=post">
						<img src="timthumb.php?src=assets/images/img03.jpg&w=470&h=340&q=100" alt="">
						<h3>Trajetória de Sucesso</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam explicabo nisi beatae accusamus sint, dicta dolorem provident doloribus non, laboriosam quidem.</p>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 wow fadeInRight">
				<div class="box-dicas">
					<a href="?pag=post">
						<img src="timthumb.php?src=assets/images/img04.jpg&w=470&h=340&q=100" alt="">
						<h3>Trajetória de Sucesso</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam explicabo nisi beatae accusamus sint, dicta dolorem provident doloribus non, laboriosam quidem.</p>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>