<section id="cases-list">
	<div class="container">
		
		<div class="row">
			<div class="box-dicas-case">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight pd0">
					<img class="imgcapa" src="assets/images/img01.jpg" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
					<h2>Maria Augusta</h2>
					<img class="aspas" src="assets/images/aspas.png" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis rerum reprehenderit, laborum corporis. Nobis a, laboriosam maxime in non qui, minus quis consectetur totam, neque voluptatum veniam libero fuga minima.</p>
					<p><a href="#">Visualizar album de fotos do caso.</a></p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="box-dicas-case">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
					<h2>Maria Augusta</h2>
					<img class="aspas" src="assets/images/aspas.png" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis rerum reprehenderit, laborum corporis. Nobis a, laboriosam maxime in non qui, minus quis consectetur totam, neque voluptatum veniam libero fuga minima.</p>
					<p><a href="#">Visualizar album de fotos do caso.</a></p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight pd0">
					<img class="imgcapa" src="assets/images/img02.jpg" alt="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="box-dicas-case">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight pd0">
					<img class="imgcapa" src="assets/images/img03.jpg" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
					<h2>Maria Augusta</h2>
					<img class="aspas" src="assets/images/aspas.png" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis rerum reprehenderit, laborum corporis. Nobis a, laboriosam maxime in non qui, minus quis consectetur totam, neque voluptatum veniam libero fuga minima.</p>
					<p><a href="#">Visualizar album de fotos do caso.</a></p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="box-dicas-case">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
					<h2>Maria Augusta</h2>
					<img class="aspas" src="assets/images/aspas.png" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis rerum reprehenderit, laborum corporis. Nobis a, laboriosam maxime in non qui, minus quis consectetur totam, neque voluptatum veniam libero fuga minima.</p>
					<p><a href="#">Visualizar album de fotos do caso.</a></p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight pd0">
					<img class="imgcapa" src="assets/images/img04.jpg" alt="">
				</div>
			</div>
		</div>

	</div>
</section>