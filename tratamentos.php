<section id="banner">
	<ul class="bxslider">
		<li style="background-image: url('assets/images/banner-tratamento.jpg');">
			<div class="sobrebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<h2>Tratamentos<br/>Odontológicos<br/>especialidades</h2>
							<p>Para Battilani experiência da estética dental é uma revolução no estilo de vida. Mais do que técnicas de alta performace, o sobrenatural é resultado de paixão pelo estado de arte.</p>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</section>
<section id="institucional">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag wow fadeInLeft">
				<img src="assets/images/icone-battilani.png"><br/>
				<h2>O dentista em Cascavel especialista em Estética Dental</h2>
			</div>
		</div>
		<div class="row ">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight">
				<h2>Experiência compartilhada</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
		</div>
	</div>
</section>