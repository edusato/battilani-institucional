<section id="banner">
	<ul class="bxslider">
		<li style="background-image: url('assets/images/banner.jpg');">
			<div class="sobrebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5">
							<h2>Valter<br/>Battilani<br/>dental beauty</h2>
							<p>Para Battilani experiência da estética dental é uma revolução no estilo de vida. Mais do que técnicas de alta performace, o sobrenatural é resultado de paixão pelo estado de arte.</p>
							<p><a class="btn-saibamais" href="#">Saiba mais sobre Battilani <i class="fas fa-chevron-right"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="background-image: url('assets/images/banner2.jpg');">
			<div class="sobrebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5">
							<h2>Maria<br/>Augusta<br/>esthetic</h2>
							<p>Para Battilani experiência da estética dental é uma revolução no estilo de vida. Mais do que técnicas de alta performace, o sobrenatural é resultado de paixão pelo estado de arte.</p>
							<p><a class="btn-saibamais" href="#">Saiba mais sobre Battilani <i class="fas fa-chevron-right"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="background-image: url('assets/images/banner3.jpg');">
			<div class="sobrebanner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5">
							<h2>José<br/>Guilhere<br/>esthetic</h2>
							<p>Para Battilani experiência da estética dental é uma revolução no estilo de vida. Mais do que técnicas de alta performace, o sobrenatural é resultado de paixão pelo estado de arte.</p>
							<p><a class="btn-saibamais" href="#">Saiba mais sobre Battilani <i class="fas fa-chevron-right"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</section>

<section id="sobre-home">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag wow fadeInLeft">
				<img src="assets/images/icone-battilani.png"><br/>
				<h2>O dentista em Cascavel especialista em Estética Dental</h2>
			</div>
		</div>
		<div class="row wow fadeInLeft">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
		</div>
		<div class="row wow fadeInRight">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 saibamais">
				<a href="#">Saiba mais sobre Battilani <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>

<section id="case-home">
	<div class="container">
		<div class="row wow fadeInLeft">
			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
				<h2>Amazing<br/>Dental<br/>Performace</h2>
				<p>Quem experimentou pode falar melhor<br/>sobre experiências e transformações</p>
				<p><a class="btn-saibamais wow fadeInRight" href="#">Cases Valter Battilani <i class="fas fa-chevron-right"></i></a></p>
			</div>
		</div>
	</div>
</section>


<section id="sobre-tratamentos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag wow fadeInLeft">
				<img src="assets/images/icone-battilani.png"><br/>
				<h2>O dentista em Cascavel especialista em Estética Dental</h2>
			</div>
		</div>
		<div class="row wow fadeInLeft">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a ullam obcaecati nobis, quasi officiis deserunt, voluptatem, alias voluptatibus assumenda expedita nam. Omnis sunt sint, repellat veritatis doloremque, quia quibusdam.</p>
			</div>
		</div>
		<div class="row wow fadeInRight">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 saibamais">
				<a href="#">Conheça os tratamentos <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>

<?php include('dicashome.php'); ?>